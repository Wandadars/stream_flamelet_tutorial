To run flamemaster on these files use:

FlameMaster -i arclength.input > UpperBranch_rightMoving.log 2>&1 &
FlameMaster -i arclength.input > UpperBranch_leftMoving.log  2>&1 &
FlameMaster -i arclength.input > LowerBranch_rightPoint.log  2>&1 &


To run ListTool to process data in this directory, transfer all flamelets to the testing directory and run:  
ListTool -s syms -r scurve.txt combined_flamelets/N-C12H26_p*

This will output a file called scurve.txt that contains data for the variable names listed in the sysm text filee


To plot output from the ListTool output file use:
python Loci-Stream/flamelet/utilities/flamemaster-tools/flamelet_scurve_plot.py  scurve.txt


To convert the ascii flamelet table to binary and remove a species(usually argon) from the table use:
asciiToBinary -x Y_AR -cp -fpvcc -f RP2.dat


Once you have the input file to the Flamelet Table Generation Tool script call it using:
python stream-flamelet/flamelet_tool/main.py input.dat


If you need to add new values of S to fill out the S-curve generate them using:
python Loci-Stream/flamelet/utilities/flamemaster-tools/flamemaster_s_values_generator.py


** A Note on Pruning **
FlameMaster often will generate a large number of flamelet solutions, which are not necesary. large clusters of solutions at the low chi_st ranges can be wiped out. Usually you want something like 10 solutions per decade of chi_st to be 'resolved'.  So it is helpful to create a separte directory called pruned_flamelets where you remove all of the extra flamelets, and then direct the flamelet table tool to that directory for generating the flamelets.
